package br.edu.fatecsjc;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class CalculadoraAluguelTest {

    private static CalculadoraAluguel calculadoraAluguel;

    // private static VerificaString verificaString;

    @BeforeAll
    static void init() {

        calculadoraAluguel = new CalculadoraAluguel(500.00);
        // verificaString = new VerificaString();
    }

    /*
    @ParameterizedTest
    @ValueSource(ints = {0, 1, 3, 5, 6, 7, 9, 10, 11, 14, 15, 16, 29, 30, 31})
    void allTests(int x) {

        double valor = calculadoraAluguel.calculoAluguel(x);

        for (int i = 0; i < x; i++) {
            if (x > 0 && x < 6)
                assertEquals(calculadoraAluguel.getAluguelNominal() * 0.9, valor, 0.0);
            else if (x == 6)
                assertEquals(calculadoraAluguel.getAluguelNominal() * 0.95 + 0.02, valor, 0.0);
            else if (x == 10)
                assertEquals(calculadoraAluguel.getAluguelNominal() * 0.95, valor, 0.0);
            else if (x >= 11 && x < 15)
                assertEquals(calculadoraAluguel.getAluguelNominal(), valor);
            else if (x >= 15 && x <= 30)
                assertEquals(calculadoraAluguel.getAluguelNominal() * 1.02
                        + calculadoraAluguel.getAluguelNominal() * 5.0E-4 * (double) (x - 15), valor);
            else if ( x < 0 || x > 30)
                assertEquals(-1.0, valor);
        }
    }



    @ParameterizedTest
    @CsvSource(value = {"-1.0:0", "450.00:1", "450.00:3", "450.00:5", "475.02:6", "475.00:7", "475.00:9", "475.00:10",
            "500.00:11", "500.00:14", "510.00:15", "510.25:16", "513.5:29", "513.75:30", "-1.0:31"}, delimiter = ':')
    void outherTests(double valor, int dia){
        assertEquals(valor, calculadoraAluguel.calculoAluguel(dia), 00);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/teste.csv", delimiter = ':')
    void outherTests2(double valor, int dia){
        assertEquals(valor, calculadoraAluguel.calculoAluguel(dia), 00);
    }


    @RepeatedTest(3)
    void authenticarTestBloqueio(RepetitionInfo repetitionInfo){

        repetitionInfo.getCurrentRepetition();
        verificaString.checar("z");

        if (repetitionInfo.getCurrentRepetition() >= 3 )
            assertTrue(verificaString.estaBloqueado());

    }

     */


    @ParameterizedTest
    @CsvFileSource(resources = "/teste2.csv")
    public void testarDivisaoPorZezoDevePassar(){

        Float valorObtido = calculadoraAluguel.divisaoPorZero(2.0f, 2.0f);
        // cc = R - N + 2

        assertEquals(1, valorObtido, 0.1f);

    }

    @Test
    public void testarDivisaoPorZeroNaoDevePassar(){

        Float valorObtido = calculadoraAluguel.divisaoPorZero(2.0f, 0.0f);
        // cc = R - N + 2

        assertEquals(0, valorObtido, 0.1f);
    }


}
