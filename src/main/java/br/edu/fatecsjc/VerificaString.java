package br.edu.fatecsjc;

public class VerificaString {

    private int contErros;

    public VerificaString(){
        this.contErros = 0;
    }

    public boolean checar(String str){
        if(!str.equals("xyz")){
            this.contErros += 1;
            return false;
        }

        return true;
    }

    public boolean estaBloqueado() {
        return contErros >= 3;
    }
}
