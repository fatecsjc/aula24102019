package br.edu.fatecsjc;

public class CalculadoraAluguel {

    private double aluguelNominal;


    public CalculadoraAluguel(double aluguelNominal){
        this.setAluguelNominal(aluguelNominal);
    }


    public double getAluguelNominal() {
        return aluguelNominal;
    }

    public void setAluguelNominal(double aluguelNominal) {
        this.aluguelNominal = aluguelNominal;
    }

    /*
    public double calculoAluguel(int diaMes){

        if (diaMes >= 1 && diaMes <= 5)
            return this.aluguelNominal * 0.90;
        else if (diaMes >= 1 && diaMes <= 10)
            return diaMes == 6 ? this.aluguelNominal * 0.95 + 0.02 : this.aluguelNominal * 0.95;
        else if (diaMes >= 1 && diaMes < 15)
            return this.aluguelNominal;
        else
            return diaMes >= 1 && diaMes <= 30 ? this.aluguelNominal * 1.02 + this.aluguelNominal * 5.0E-4 * (double) (diaMes - 15) : -1.0;

    }


     */

    public Float divisaoPorZero(Float x, Float y){

        Float returnedVal;

        try{
            returnedVal = x / y;

            if (y.equals(0.0f))
                throw new ArithmeticException();
        }catch (ArithmeticException e){
            returnedVal = 0f;
        }

        return returnedVal;
    }
}
